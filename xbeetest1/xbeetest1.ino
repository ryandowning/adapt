#include <SoftwareSerial.h>

SoftwareSerial mySerial =  SoftwareSerial(2, 3);


void setup()  {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  Serial.println("Goodnight moon!");
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  mySerial.println("Hello, world?");
}



void loop()                     // run over and over again
{

  if (mySerial.available()) {
      Serial.write(mySerial.read());
  }
  if (Serial.available()) {
      mySerial.write(Serial.read());
  }
  delay(200);
}
