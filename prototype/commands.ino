#define query_byte '?'
#define ask_byte '/'
#define set_status_byte '|'


void parseCommand(byte cmd){
	switch(cmd){
	case query_byte:
		answer();
		break;
	case ask_byte:
		ask();
		break;
	case set_status_byte:
		set_status();
		break;
	case 'm':
		drive((double) readNum(), (double) readNum(), (double) readNum());
		break;
	case 'n':
		drive(0,0,0);
		break;
	default: 
		xbee.write(cmd);
		break;
	}
}

void answer(){
	while(cmdQ.isEmpty())
		xbeelisten();
	byte questioner = cmdQ.pop();
	sendByte(questioner, status);
	 
}

byte ask(){
	while(cmdQ.isEmpty())
		xbeelisten();
	byte target = cmdQ.pop();
	sendByte(target, query_byte);
	sendByte(target, r_ID);
	while(cmdQ.isEmpty())
		xbeelisten();
	return cmdQ.pop();
}

void set_status(){
	while(cmdQ.isEmpty())
		xbeelisten();
	status = cmdQ.pop();
}