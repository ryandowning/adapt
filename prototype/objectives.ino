void goToGreen(){
	camtrack(green);
	if(t_data.pixels<4) drive(0,0,0);
	else{
		drive(1,0,0);
		if(t_data.mx<65) drive(0,0,1);
		if(t_data.mx>95) drive(0,0,-1);


	}
}

void pcOrbit(double od){
	double kp = -5;
	drive(kp*(b+dist(1)-od), 1, 1/od);
}

void pcOrbit2(double od){
	double dL = dist(Lsensor), dR = dist(Rsensor);
	double xm = od - (dL+dR)/2;
	double Tm = atan((dR-dL)/sensorSpace);
	drive(xm, 1, Tm);
}