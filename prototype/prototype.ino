#include "Queuelist.h"
#include <SoftwareSerial.h>
#include <CMUcam4.h>
#include <CMUcom4.h>
#include <EEPROM.h>
#include "EEPROM_Uni.h"

////debugging, can deactivate preprocessor blocks
//#define using_cam

////pins
//wheels w,num,forward/reverse
#define w1f 3
#define w1r 5
#define w2f 6
#define w2r 9
#define w3f 10
#define w3r 11
//xbee sw serial
#define xbeerx 2
#define xbeetx 7
//distance sensor
#define Lsensor 1
#define Rsensor 2

////measurements
#define b .12 //distance from center of robot to wheel
#define sensorSpace .04 //distance b/w sensors

////hardware vars
//camera
CMUcam4 cam(CMUCOM4_SERIAL);
CMUcam4_tracking_data_t t_data;
//xbee serial
SoftwareSerial xbee =  SoftwareSerial(xbeerx, xbeetx);
//wheel pin array
int wheel[2][3] = {{w1f, w2f, w3f}, {w1r, w2r, w3r}};

//robot's unique identifier byte, 'r_ID', stored in position 0 of EEPROM
byte r_ID = EEPROM.read(0);
byte status = '0';

//queue of recieved command information
QueueList <byte> cmdQ;


int green[6] = {0, 100, 100, 200, 50, 150};//yuv mode

void setup()
{
	delay(10);
	drive(0,0,0);
	Serial.begin(9600);
	// set the data rate for the SoftwareSerial port
	xbee.begin(19200);

#ifdef using_cam
	cam.begin();				//initialize camera
	cam.LEDOn(5);
	delay(5000);				//camera auto-adjustments
	cam.autoGainControl(false);
	cam.autoWhiteBalance(false);
	cam.LEDOn(CMUCAM4_LED_ON);
#endif	//using_cam
}//setup

byte in = '0'; byte stuff;
int s;
bool forwarding = false;
void loop()

{
	xbeelisten();
	/*while(!cmdQ.isEmpty()){
		//xbee.write(cmdQ.pop());
		switch(cmdQ.pop()){
		case 'r': sendPriorityBytes('@', (byte*)"ryan", 4);
			break;
		default: sendBytes('@', (byte*)"hello", 5);
			break;
		}
	}*/
	while(!cmdQ.isEmpty())
		//parseCommand(cmdQ.pop());
			drive(0,0,1);




	//controller
	/*
	while(!cmdQ.isEmpty()){
		stuff = cmdQ.pop();
		if(stuff=='f')
			forwarding = !forwarding;
		if(forwarding&&stuff!='f')
			while(!sendByte('!', stuff));
	}
	*/
	//xbee.write(cmdQ.pop());
	
	//driver
	/*
	if(!cmdQ.isEmpty())
		switch(cmdQ.pop()){
		case 's': drive(0,0,0);
			break;
		case 'w': drive(1,0,0);
			break;
		case 'a': drive(0,-1,0);
			break;
		case 'd': drive(0,1.0,0);
			break;
		case 'x': drive(-1,0,0);
			break;
		case 'r': drive(0,0,1);
			break;
		case 'v': drive(0,0,-1);
			break;
		case 'q': drive(1, -1, 0);
	}
	
	Rdelay(5000);
	drive(0,0,0);
	*/
}//loop

