/** Normalizes matrix n of length 3
*/
void norm(double n[]){
	double nd = sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);	//sums the squares of the 3 wheel speeds into nd
	if(nd)						//if nd is nonzero
		for(int i=0; i<3; i++) 
			n[i]/=nd;					//divide each wheel spead by nd
}

/** make robot drive with motion xdot (x, y, theta)
+x is forward, +y is left +theta is ccw  */
void drive(double dx, double dy, double dTheta){ // wheel pin control fxn

	double xdot[] = {dx, dy, dTheta};	//store x', y', and theta' in matrix xdot
	double w[3]= {0, 0, 0};				//matrix of wheel velocities

	//calculate wheel velocities using transformation matrix for kiwi drive
#define c1 .57735 //1/sqrt(3)

	w[0] = c1*xdot[0] - xdot[1]/3 - b*xdot[2];
	w[1] = -c1*xdot[0] - xdot[1]/3 - b*xdot[2];
	w[2] = 2*xdot[1]/3 - b*xdot[2];

	norm(w);	//normalize wheel speeds

	for(int i=0; i<3; i++){    //for each wheel
		if(w[i]>=0){	//if positive velocity
			analogWrite(wheel[0][i], round(255*w[i]));	//analog write velocity to forward wheel pin
			analogWrite(wheel[1][i], 0);				//write 0 to reverse wheel pin	
		}
		else{			//negative or 0 velocity
			analogWrite(wheel[1][i], -round(255*w[i]));	//analog write -velocity to reverse wheel pin
			analogWrite(wheel[0][i], 0);				//write 0 to froward wheel pin
		}
	}//for
}//drive
