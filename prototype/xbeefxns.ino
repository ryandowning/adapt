/*version 1, has listen state that toggles on and off for r_ID
 - would be useful for sending long streams of bytes
 - unfortunately, succeptible to interference from any other communication
*/
/*bool listen(){
	
	char in;
	if (xbee.available()) {
		in= xbee.read();
		if(in==r_ID){
			//mySerial.write(r_ID);
			while(1){
				while(!xbee.available());
				if((in = xbee.read())==r_ID){ 
					//mySerial.write(r_ID);
					return true;
				}
				cmdQ.push(in);
			}
		}
	}
	return false;
}*/

/* version 3, data packets composed of multiple bytes 
 - good for large networks or complex commands
 - however, this induces to many unnecesary delays
 - multiple ways to implement, but could be costly for memory
 - instead will use single byte transmissions, 
 - up to internal programming to interpret bytes if they are meant to be strung together
 */

/*
#define packet_size 4
struct pkt{
	char d[packet_size];
	bool operator==(const char* rhs){ return !strcmp(this->d, rhs);}
};

QueueList <pkt> plist;

void xbeepacket(){
	xbee.setTimeout(2000);
	pkt p;
	p.d[packet_size] = '\0';
	p.d[xbee.readBytes(p.d, packet_size-1)];
	plist.push(p);
	if(p=="abc")
		//xbee.write(p.d);
		while(!plist.isEmpty())
			xbee.write(plist.pop().d);
}*/


#define timeout 0			//command timeout in ms
#define confirm_byte ':'	//byte to send in response to r_ID
#define stop_byte '.'
#define override_byte '~'
#define priority_byte '<'

////gets all data from xbee buffer, stores data if addressed to this robot's r_ID
bool listening = false;			//global bool -- is robot currently listening
bool priority_cmd = false;
void xbeelisten(){					
	byte inc;
	while(xbee.available()){	//repeat until xbee buffer is empty
		inc = xbee.read();
		if(inc==confirm_byte) continue;
		if(inc==r_ID){				//if addressing this robot's r_ID
			xbee.write(confirm_byte);		//echo confirmation byte
			listening = true;		//enable listening
		}else if(listening){		//if already listening
			if(inc==priority_byte)		//if sending priority command prefix
				priority_cmd = true;		//listen for priority command
			if(inc!=stop_byte&&inc!=priority_byte)		//checks for cancel byte
				if(priority_cmd){			//if priority command
					cmdQ.pushfront(inc);		//insert command in front
					priority_cmd = false;		//stop listening for priority command
				}
				else					//regular command
					cmdQ.push(inc);			//push byte into queue
			if(inc==override_byte)
				cmdQ.clear();
			listening=false;		//stop listening
		}
	}//while
}//xbeelisten

////transmits command byte to robot with r_ID target
bool sendByte(byte target, byte command){
	xbeelisten();					//clear buffer
	xbee.write(target);				//address target's r_ID
	int t=timeout;					//timeout in ms
	while(!xbee.available()&&t){	//wait for confirmation byte or time runs out
		t--;							//countdown timeout
		delay(1);						//delay 1 ms
	}
	if(!t&&timeout){							//timed out
		xbee.write(stop_byte);
		return false;					//failed
	}
	if(xbee.read()==confirm_byte)	//if did not time out, and received confirmation byte
		xbee.write(command);				//send command
	return true;						//success
}

bool sendPriorityByte(byte target, byte command){
	if(!sendByte(target, priority_byte))
		return false;
	return sendByte(target, command);
}

bool sendBytes(byte target, byte* bytes, int numBytes){
	for(int i=0; i<numBytes; i++)
		if(!sendByte(target, bytes[i])) return false;
	return true;
}

bool sendPriorityBytes(byte target, byte* bytes, int numBytes){
	for(int i=numBytes; i>0; i--)
		if(!sendPriorityByte(target, bytes[i-1])) return false;
	return true;
}


bool sendNum(byte target, int num){
	byte digits[3];
	if(num<0){
		sendByte(target, '-');
		num = -num;
	}
	digits[0] = num/100 + '0';
	digits[1] = (num%100)/10 + '0';
	digits[2] = num%10 + '0';
	sendBytes(target, digits, 3);
}

int readNum(){
	int num = 0;
	while(cmdQ.isEmpty())
		xbeelisten();
	bool neg = cmdQ.peek()=='-';
	if(neg) cmdQ.pop();
	for(int i=0; i<3; i++){
		while(cmdQ.isEmpty())
			xbeelisten();
		num = 10*num + cmdQ.pop() - '0';
	}
	if(neg) return -num;
	return num;
}

////delays for time _T in ms while simultaneously clearing xbee buffer
void Rdelay(unsigned long _T){
	unsigned long start = millis();
	while(millis()-start < _T)
		xbeelisten();
}
