//adapted from http://forum.arduino.cc/index.php/topic,41497.0.html

void EEPROM_WriteDouble(int ee, const double& value)
{
	for (int i = 0; i < sizeof(value); i++)
		EEPROM.write(ee+i, *((byte*)(void*)&value+i));
}

double EEPROM_ReadDouble(int ee)
{
	double value = 0;
	for (int i = 0; i < sizeof(value); i++)
		*((byte*)(void*)&value+i) = EEPROM.read(ee++);
	return value;
}
