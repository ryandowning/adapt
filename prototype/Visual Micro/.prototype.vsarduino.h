//Board = Arduino Uno
#define ARDUINO 103
#define __AVR_ATmega328P__
#define F_CPU 16000000L
#define __AVR__
#define __cplusplus
#define __attribute__(x)
#define __inline__
#define __asm__(x)
#define __extension__
#define __ATTR_PURE__
#define __ATTR_CONST__
#define __inline__
#define __asm__ 
#define __volatile__
#define __builtin_va_list
#define __builtin_va_start
#define __builtin_va_end
#define __DOXYGEN__
#define prog_void
#define PGM_VOID_P int
#define NOINLINE __attribute__((noinline))

typedef unsigned char byte;
extern "C" void __cxa_pure_virtual() {}

//already defined in arduno.h
//already defined in arduno.h
void EEPROM_WriteDouble(int ee, const double& value);
double EEPROM_ReadDouble(int ee);
void camtrack(int cr[]);
void parseCommand(byte cmd);
void answer();
byte ask();
void set_status();
void goToGreen();
void pcOrbit(double od);
void pcOrbit2(double od);
double dist(int dsens);
void norm(double n[]);
void drive(double dx, double dy, double dTheta);
void xbeelisten();
bool sendByte(byte target, byte command);
bool sendPriorityByte(byte target, byte command);
bool sendBytes(byte target, byte* bytes, int numBytes);
bool sendPriorityBytes(byte target, byte* bytes, int numBytes);
bool sendNum(byte target, int num);
int readNum();
void Rdelay(unsigned long _T);

#include "C:\Program Files (x86)\arduino\hardware\arduino\variants\standard\pins_arduino.h" 
#include "C:\Program Files (x86)\arduino\hardware\arduino\cores\arduino\arduino.h"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\prototype.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\EEPROM_Uni.h"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\EEPROM_Uni.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\Queuelist.h"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\camfxns.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\commands.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\objectives.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\sensorfxns.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\wheels.ino"
#include "C:\Users\red_0_000\SkyDrive\workspace\arduino\adapt\prototype\xbeefxns.ino"
