/* Universal Read/write functions for EEPROM
- Modified 8/12/13 by Ryan Downing
- adapted from http://forum.arduino.cc/index.php/topic,41497.0.html
*/

template <class T> void EEPROM_WriteUni(int ee, const T& value)
{
	for (int i = 0; i < sizeof(value); i++)
		EEPROM.write(ee+i, *((byte*)(void*)&value+i));
}

template <class T> void EEPROM_ReadUni(int ee, T & value)
{
	for (int i = 0; i < sizeof(value); i++)
		*((byte*)(void*)&value+i) = EEPROM.read(ee++);
}
