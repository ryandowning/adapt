const byte DRIVE1_CW = 4;
const byte DRIVE1_CCW = 5;
const byte DRIVE2_CW = 6;
const byte DRIVE2_CCW = 7;
const byte DRIVE3_CW = 8;
const byte DRIVE3_CCW = 9;
const byte MANIPULATOR_UP = 12;
const byte MANIPULATOR_DOWN = 13;

void setup() {
  pinMode(DRIVE1_CW, OUTPUT);
  pinMode(DRIVE1_CCW, OUTPUT);
  pinMode(DRIVE2_CW, OUTPUT);
  pinMode(DRIVE2_CCW, OUTPUT);
  pinMode(DRIVE3_CW, OUTPUT);
  pinMode(DRIVE3_CCW, OUTPUT);
  pinMode(MANIPULATOR_UP, OUTPUT);
  pinMode(MANIPULATOR_DOWN, OUTPUT);
  reset();
  Serial.begin(19200);
  Serial.println("Begin.");
}

void loop() {
  while (Serial.available()) {
    char cmd = Serial.read();
    switch (cmd) {
      case 'd': circle(true, 50); break;
      case 'a': circle(false, 50); break;
      case 'w': manipulator(true, 50); break;
      case 's': manipulator(false, 50); break;
      case 'k': straight(true, 50); break;
      case 'i': straight(false, 50); break;
      case 'j': sideways(true, 50); break;
      case 'l': sideways(false, 50); break;
      case 'q': testall(); break;
      case ' ': reset(); break;
      default: Serial.println("Unknown input."); break;
    }
  }
}

void circle(boolean clockwise, long duration) {
  Serial.print("Rotating in a circle for ");
  Serial.println(duration);
  Serial.print("Clockwise? ");
  Serial.println(clockwise);
  if (clockwise) {
//    unsigned long time = millis();
//    while (time + duration > millis()) {
//      digitalWrite(DRIVE1_CW, !digitalRead(DRIVE1_CW));
//      digitalWrite(DRIVE2_CW, !digitalRead(DRIVE2_CW));
//      digitalWrite(DRIVE3_CW, !digitalRead(DRIVE3_CW));
//      delay(5);
//    }
    digitalWrite(DRIVE1_CW, HIGH);
    digitalWrite(DRIVE2_CW, HIGH);
    digitalWrite(DRIVE3_CW, HIGH);
    delay(duration);
    digitalWrite(DRIVE1_CW, LOW);
    digitalWrite(DRIVE2_CW, LOW);
    digitalWrite(DRIVE3_CW, LOW);
  }
  else {
//    unsigned long time = millis();
//    while (time + duration > millis()) {
//      digitalWrite(DRIVE1_CCW, !digitalRead(DRIVE1_CCW));
//      digitalWrite(DRIVE2_CCW, !digitalRead(DRIVE2_CCW));
//      digitalWrite(DRIVE3_CCW, !digitalRead(DRIVE3_CCW));
//      delay(5);
//    }
    digitalWrite(DRIVE1_CCW, HIGH);
    digitalWrite(DRIVE2_CCW, HIGH);
    digitalWrite(DRIVE3_CCW, HIGH);
    delay(duration);
    digitalWrite(DRIVE1_CCW, LOW);
    digitalWrite(DRIVE2_CCW, LOW);
    digitalWrite(DRIVE3_CCW, LOW);
  }
}

void manipulator(boolean up, long duration) {
  Serial.print("Moving manipulator for ");
  Serial.println(duration);
  Serial.print("Up? ");
  Serial.println(up);
  if (up) {
    digitalWrite(MANIPULATOR_UP, HIGH);
    delay(duration);
    digitalWrite(MANIPULATOR_UP, LOW);
  }
  else {
    digitalWrite(MANIPULATOR_DOWN, HIGH);
    delay(duration);
    digitalWrite(MANIPULATOR_DOWN, LOW);
  }
}

void straight(boolean forward, long duration) {
  Serial.print("Moving in a straight line for ");
  Serial.println(duration);
  Serial.print("Forward? ");
  Serial.println(forward);
  if (forward) {
    digitalWrite(DRIVE1_CW, HIGH);
    digitalWrite(DRIVE2_CCW, HIGH);
    delay(duration);
    digitalWrite(DRIVE1_CW, LOW);
    digitalWrite(DRIVE2_CCW, LOW);
  }
  else {
    digitalWrite(DRIVE1_CCW, HIGH);
    digitalWrite(DRIVE2_CW, HIGH);
    delay(duration);
    digitalWrite(DRIVE1_CCW, LOW);
    digitalWrite(DRIVE2_CW, LOW);
  }
}

void sideways(boolean left, long duration) {
  if (left) {
    digitalWrite(DRIVE3_CW, HIGH);
    unsigned long time = millis();
    while (time + duration > millis()) {
        digitalWrite(DRIVE1_CCW, !digitalRead(DRIVE1_CCW));
        digitalWrite(DRIVE2_CCW, !digitalRead(DRIVE2_CCW));
        delay(1);
    }
    digitalWrite(DRIVE1_CCW, LOW);
    digitalWrite(DRIVE2_CCW, LOW);
    digitalWrite(DRIVE3_CW, LOW);
  }
  else {
    digitalWrite(DRIVE3_CCW, HIGH);
    unsigned long time = millis();
    while (time + duration > millis()) {
        digitalWrite(DRIVE1_CW, !digitalRead(DRIVE1_CW));
        digitalWrite(DRIVE2_CW, !digitalRead(DRIVE2_CW));
        delay(1);
    }
    digitalWrite(DRIVE1_CW, LOW);
    digitalWrite(DRIVE2_CW, LOW);
    digitalWrite(DRIVE3_CCW, LOW);
  }
}

void reset() {
  Serial.println("Resetting.");
  digitalWrite(DRIVE1_CW, LOW);
  digitalWrite(DRIVE1_CCW, LOW);
  digitalWrite(DRIVE2_CW, LOW);
  digitalWrite(DRIVE2_CCW, LOW);
  digitalWrite(DRIVE3_CW, LOW);
  digitalWrite(DRIVE3_CCW, LOW);
  digitalWrite(MANIPULATOR_UP, LOW);
  digitalWrite(MANIPULATOR_DOWN, LOW);
}

void testall() {
  circle(true, 6000);
  delay(1000);
  circle(false, 6000);
  delay(1000);
  manipulator(true, 5000);
  delay(1000);
  manipulator(false, 5000);
  delay(1000);
  straight(true, 2000);
  delay(1000);
  straight(false, 2000);
  delay(1000);
  sideways(true, 2000);
  delay(1000);
  sideways(false, 2000);
  delay(1000);
}


