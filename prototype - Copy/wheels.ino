void norm(double n[]){
	double nd = n[0]*n[0]+n[1]*n[1]+n[2]*n[2];
	for(int i=0; i<3; i++) n[i]/=nd;
}

/** make robot drive with motion xdot (x, y, theta)
+x is forward, +y is right +theta is ccw  */
void drive(double dx, double dy, double dTheta){ // wheel pin control fxn
	double xdot[] = {dx, dy, dTheta};
	//digitalWrite(2, HIGH);
	double w[3]= {0, 0, 0};
	w[0] = .5774*xdot[0] + xdot[1]/3 - b*xdot[2];
	w[1] = -.5774*xdot[0] + xdot[1]/3 - b*xdot[2];
	w[2] = -2*xdot[1]/3 - b*xdot[2];
	Serial.print(w[1], DEC);
	norm(w);
	for(int i=0; i<3; i++){    //analog write speeds to wheel pins
		if(w[i]>=0){
			analogWrite(wheel[0][i], round(255*w[i]));
			analogWrite(wheel[1][i], 0);
		}
		else{
			analogWrite(wheel[1][i], -round(255*w[i]));
			analogWrite(wheel[0][i], 0);
		}
	}//for
}//drive
