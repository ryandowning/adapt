bool listen(){
	
	char in;
	if (xbee.available()) {
		in= xbee.read();
		if(in==r_ID){
			//mySerial.write(r_ID);
			while(1){
				while(!xbee.available());
				if((in = xbee.read())==r_ID){ 
					//mySerial.write(r_ID);
					return true;
				}
				cmdQ.push(in);
			}
		}
	}
	return false;
	
}

#define timeout 100			//command timeout in ms
#define confirm_byte '?'	//byte to send in response to r_ID
#define cancel_byte '~'
#define override_byte '.'
#define priority_byte '<'

////gets all data from xbee buffer, stores data if addressed to this robot's r_ID
bool listening = false;			//global bool -- is robot currently listening
bool priority_cmd = false;
void xbeelisten(){					
	byte inc;
	while(xbee.available()){	//repeat until xbee buffer is empty
		inc = xbee.read();
		if(inc==r_ID){				//if addressing this robot's r_ID
			xbee.write(confirm_byte);		//echo confirmation byte
			listening = true;		//enable listening
		}else if(listening){		//if already listening
			if(inc==priority_byte)		//if sending priority command prefix
				priority_cmd = true;		//listen for priority command
			if(inc!=cancel_byte&&inc!=priority_byte)		//checks for cancel byte
				if(priority_cmd){			//if priority command
					cmdQ.pushfront(inc);		//insert command in front
					priority_cmd = false;		//stop listening for priority command
				}
				else					//regular command
					cmdQ.push(inc);			//push byte into queue
			if(inc==override_byte)
				cmdQ.clear();
			listening=false;		//stop listening
		}
	}//while
}//xbeelisten

////transmits command byte to robot with r_ID target
bool sendCMD(byte target, byte command){
	xbeelisten();					//clear buffer
	xbee.write(target);				//address target's r_ID
	int t=timeout;					//timeout in ms
	while(!xbee.available()&&t){	//wait for confirmation byte or time runs out
		t--;							//countdown timeout
		delay(1);						//delay 1 ms
	}
	if(!t){							//timed out
		xbee.write(cancel_byte);
		return false;					//failed
	}
	if(xbee.read()==confirm_byte)	//if did not time out, and received confirmation byte
		xbee.write(command);				//send command
	return true;						//success
}

bool sendPriorityCMD(byte target, byte command){
	if(!sendCMD(target, priority_byte))
		return false;
	return sendCMD(target, command);
}

////delays for time _T in ms while simultaneously clearing xbee buffer
void Rdelay(unsigned long _T){
	unsigned long start = millis();
	while(millis()-start < _T)
		xbeelisten();
}


void xbeepacket(){
	xbee.setTimeout(2000);
	char pack[4];
	pack[3] = '\0';
	xbee.readBytes(pack, 3);
		if(strcmp(pack, "abc")==0)
			xbee.write(pack);
	/*
	for(int i=0; i<3; i++){
		while(!xbee.available()) if("abc"=="axc")xbee.write(")%\0");
		xbee.write(xbee.read());
	}*/
	
}