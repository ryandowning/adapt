#include <SoftwareSerial.h>

// Transmission format:
// START_TRANSMISSION&ID   START_MSG&ID RECIPIENTID START_ARRAY STATUS STATEARRAY END_MSG
// 10 bytes (1, then 9)
// First byte is to state intention to talk, then the next 9 are the message

// Constants and data
const byte ID = 10; // identifies this specific robot
const byte UNIVERSAL_ID = 0; //message is for all robots
const byte START_TRANSMISSION = B11000000; // Added to ID, indicates the robot wants to send a message
const byte START_MSG = B10100000; // Added to ID when sending messages
const byte END_MSG = B11100000; // Completes a message
const byte START_ARRAY = B10000001; // Indicates that state and status arrays will be sent
const byte START_OTHER = B10000010; // Indicates that something other data will occur; these indicate the topic of the message

// Constants for me to set
const byte SEND_DELAY = 5; // Delay 5 ms between each sent byte
const int RECEIVE_TIMEOUT = 30; // Maximum allowable time between two consecutive bytes received
const byte MODULE_COUNT = 6; // Number of modules in the robots, 1-8
const int SENDING_DELAY = 100; // Delay between subsequent broadcasts
const int CONFLICT_DELAY = 15; // Delay to allow for two robots broadcasting at the same time
boolean broadcast = true; // For debugging

// XBee variables
SoftwareSerial xbee(2, 3);
// SENDING
byte sendBuffer[16]; // For sending data; can send up to 16 bytes at once
byte sendBufferByte = 0; // Current byte that we're sending from the buffer, 0-15
byte sendBufferSize = 0; // Number of chars to send from the buffer
unsigned long lastSendTime = 0; // Time of last byte sent
boolean wantToSend = false; // Whether or not we current have data to send
byte nextToSend = 255; // ID who will be sending next, used to determine priority
byte stateArray[] = {39, 67, 15, 82}; // All values must be less than 192
byte statusArray = B000000; // Status of each module, 1 = bad and 0 = good
// RECEIVING
byte receiveBuffer[16]; // Read data into here until there's an END_MSG
byte receiveBufferByte = 0; // Current byte that we're writing to in the buffer, 0-15
byte receivingFrom = 0; // ID of the robot that the transmission is from
unsigned long lastReceiveTime = 0; // Time of last byte read
boolean currentlyReading = false; // Whether or not we are currently reading data


void setup() {
  Serial.begin(57600);
  xbee.begin(57600);
  Serial.println("Ready.");
  pinMode(13, OUTPUT); // Use 13 to indicate reading/writing to xbee, for debugging
}

void loop() {
  // SoftwareSerial stops listening occasionally; calling it every
  // loop will ensure that it's always listening. This is a poor
  // hack, a better solution is needed.
  xbee.listen();
  
  // If mySerial is available, read the next character(s)
  if (xbee.available()) {
//    Serial.print("xbee available ");
//    Serial.println(xbee.available());
    readChar();
  }
  // If I'm currently reading something but haven't received anything recently,
  // stop listening for the message and let myself send if I want
  if (currentlyReading && (millis() - lastReceiveTime > RECEIVE_TIMEOUT)) {
    Serial.println("Receive error: message timed out");
    endReceive(true);
  }
  
  // If we want to send data, and aren't currently receiving data, then start sending
  if (wantToSend) {
//    Serial.println("wantToSend");
    trySendChar();
  }
  
  // Try to send my data every few seconds
  if ((millis() - lastSendTime > SENDING_DELAY) & broadcast) {
//    Serial.println("sendArrays");
    sendArrays();
  }
  
  // Parse user input, for testing
  if (Serial.available()) {
    switch (Serial.read()) {
      case 'a': sendArrays(); break;
      case 'b': broadcast = !broadcast; break;
      case 'c': printAll(); break;
      case 'd': asm volatile ("  jmp 0"); break; // Soft reset, it's saying "Jump to line 0 in assembly code"
      default: break;
    }
  }
}

// Debugging function, prints out all variables and their values
void printAll() {
  Serial.print("broadcast = ");
  Serial.println(broadcast);
  Serial.print("sendBuffer = ");
  for (int i = 0; i<sizeof(sendBuffer); i++) {
    Serial.print(sendBuffer[i]);
    Serial.print(" ");
  }
  Serial.println("");
  Serial.print("sendBufferByte = ");
  Serial.println(sendBufferByte);
  Serial.print("sendBufferSize = ");
  Serial.println(sendBufferSize);
  Serial.print("millis() - lastSendTime = ");
  Serial.println(millis()-lastSendTime);
  Serial.print("wantToSend = ");
  Serial.println(wantToSend);
  Serial.print("nextToSend = ");
  Serial.println(nextToSend);
  Serial.print("receiveBuffer = ");
  for (int i = 0; i<sizeof(receiveBuffer); i++) {
    Serial.print(receiveBuffer[i]);
    Serial.print(" ");
  }
  Serial.println("");
  Serial.print("receiveBufferByte = ");
  Serial.println(receiveBufferByte);
  Serial.print("receivingFrom = ");
  Serial.println(receivingFrom);
  Serial.print("millis() - lastReceiveTime = ");
  Serial.println(millis()-lastReceiveTime);
  Serial.print("currentlyReading = ");
  Serial.println(currentlyReading);
  Serial.print("xbee.peek() = ");
  Serial.println((byte) xbee.peek());
  Serial.print("xbee.available() = ");
  Serial.println(xbee.available());
  Serial.print("xbee.overflow() = ");
  Serial.println(xbee.overflow());
}

/*
* Reads one character from the XBee
*/
void readChar() {  
//  Serial.print("Byte received: ");
//  Serial.println((byte) xbee.peek());
  // Just ignore it if it's a 255
  if ((byte) xbee.peek() == 255) {
    xbee.read();
    return;
  }

  digitalWrite(13, HIGH);
  currentlyReading = true; // Set false when we encounter an END_MSG, or time out
  
  // Starting a transmission
  if ((byte) xbee.peek()/32 == (byte) B110) {
    Serial.print("Robot ");
    Serial.print((byte) xbee.peek() % 64);
    Serial.println(" is trying to start a transmission.");
    // Assume the lowest ID will be sending the message
    if (xbee.peek() % 64 < nextToSend) {
      nextToSend = xbee.read() % 64;
    }
//    Serial.println(nextToSend);
    lastReceiveTime = millis();
  }
  // Otherwise, it's an actual message and we should save to buffer
  else {
//    Serial.println("Receiving a message.");
    receiveBuffer[receiveBufferByte] = xbee.read();
    if (receiveBufferByte == 0) {
      // Check that if it's the first char, it's a START_MSG char
      if ((byte) receiveBuffer[receiveBufferByte]/32 != (byte) B101) {
        Serial.print("Receiving error: ignoring character ");
        Serial.println((byte) receiveBuffer[receiveBufferByte]);
        endReceive(false);
        return;
      }
    }
    // If it's the last character, act on it
    if ((byte) receiveBuffer[receiveBufferByte] == (byte) END_MSG) {
      endReceive(false);
    }
    // Otherwise, increment
    else {
      receiveBufferByte++;
      lastReceiveTime = millis();
    }
  }
  digitalWrite(13, LOW);
}

/*
* Called when we finish reading a message
*/
void endReceive(boolean timeout) {
//  Serial.println("Finished receiving a message.");
  // If no timeout, parse the message
  if (!timeout) {
    parseMessage();
  }
  // Clear the variables once parsing has completed
  for (int i = 0; i < receiveBufferByte; i++) {
    receiveBuffer[i] = 0;
  }
  receiveBufferByte = 0;
  receivingFrom = 0;
  currentlyReading = false;
}

/*
* Parses the message to determine its sender, receiver, and type.
* Then calls on other methods to interpret the data as needed.
*/
void parseMessage() {
  // First check that the message is for me
  if (receiveBuffer[1] == ID || (receiveBuffer[1] == UNIVERSAL_ID && receiveBufferByte > 1)) {
//    Serial.println("Message is for me.");
    // Then record who it is from
    receivingFrom = receiveBuffer[0] % 32;
    // In theory we can have many different message types; currently it's
    // just sending the status/state arrays
    switch (receiveBuffer[2]) {
      case (START_ARRAY): {
        if (receiveBufferByte == 8) {
          readStatus();
          readState();
        }
        else {
          Serial.println("Receive error: incorrect message length.");
        }
        break;
      }
      default: {
        Serial.println("Receive error: unknown message type");
        break;
      }
    }
  }
  // Otherwise the message is for a different robot
  else {
    Serial.print("Ignoring message for robot ");
    Serial.println(receiveBuffer[1]);
  }
}

/*
* Reads the status of the other robot
*/
void readStatus() {
  // For now, just print out to say that something is broken in the given robot
  for (int i = 0; i < MODULE_COUNT; i++) {
    if (bitRead(receiveBuffer[3], i) == 1) {
      Serial.print("Module ");
      Serial.print(i);
      Serial.print(" is broken in robot ");
      Serial.println((byte) receivingFrom);
    }
  }
}

/*
* Reads the state of the other robot
*/
void readState() {
  // For now, just print out received data.
  // TODO: Use this data along with maximum allowable
  // times to determine whether the robot is taking too long
  Serial.print("Received state array from robot ");
  Serial.print((byte) receivingFrom);
  Serial.print(": ");
  for (int i = 0; i < sizeof(stateArray); i++) {
    Serial.print((byte) receiveBuffer[4+i]);
    Serial.print(" ");
  }
  Serial.println("");
}

/*
* Main function for sending message of XBee
*/
void trySendChar() {
  // We need to wait before sending the 2nd byte, to make sure there are no conflicts
  if (sendBufferByte == 1) {
    // If it's >15ms, then figure out if we should start sending
    if (millis() - lastSendTime > CONFLICT_DELAY) {
      // If nobody else tried to send, then continue sending
      if (!currentlyReading) {
//        Serial.println("It's been 15ms, and nobody else tried to send data.");
        sendChar();
      }
      // Otherwise, figure out who should be sending data
      else {
//        Serial.println("Conflict with another robot in sending data. Let's figure this out...");
//        Serial.println("#########################");
//        Serial.println("#########################");
//        Serial.println("#########################");
//        Serial.println("#########################");
//        Serial.println("#########################");
        sendingConflict();
      }
    }
  }
  else if (!currentlyReading) {
    // Need to wait 5ms between sending each char
    if (millis() - lastSendTime > 5) {
      sendChar();
    }
  }
  // Once we send everything, stop
  if (sendBufferByte == sendBufferSize) {
    endSend();
  }
}

/*
* Sends one character over xbee
*/
void sendChar() {
//  Serial.print("Sending byte ");
//  Serial.println(sendBuffer[sendBufferByte]);
  digitalWrite(13, HIGH);
  xbee.write(sendBuffer[sendBufferByte]);
  sendBufferByte++;
  lastSendTime = millis();
  digitalWrite(13, LOW);
}

/*
* Resvolves conflicts if there are multiple robots sending at the same time
*/
void sendingConflict() {
//  Serial.println("Sending conflict.");
  // If nextToSend is 255, then there's been a problem with the transmission
  // and I should just wait.
  if (ID < nextToSend && nextToSend != 255) {
    // My turn to send
    //Serial.println(nextToSend);
    endReceive(true);
    nextToSend = 255;
//    Serial.println("I win.");
  }
  else {
    // Wait until my turn
    sendBufferByte = 0;
    nextToSend = 255;
//    Serial.println("I lose.");
  }
}

/*
* Ends the process of sending
*/
void endSend() {
  for (int i = 0; i < sendBufferSize; i++) {
    sendBuffer[i] = 0;
  }
  sendBufferByte = 0;
  sendBufferSize = 0;
  wantToSend = false;
  Serial.println("Finished sending my data.");
}

/*
* Sets it up so the status/state arrays will be sent
*/
void sendArrays() {
  // Only do this if we aren't already trying to send data.
  if (!wantToSend) {
//    Serial.println("Attempting to send my arrays.");
    // Put data in the sendingBuffer
    sendBuffer[0] = START_TRANSMISSION+ID;
    sendBuffer[1] = START_MSG+ID;
    sendBuffer[2] = UNIVERSAL_ID; // Recipient ID; for now assume it's everyone
    sendBuffer[3] = START_ARRAY;
    sendBuffer[4] = statusArray;
    for (int i = 0; i < sizeof(stateArray); i++) {
      sendBuffer[i+5] = stateArray[i];
    }
    sendBuffer[5+sizeof(stateArray)] = END_MSG;
    // Update other variables
    sendBufferSize = 6+sizeof(stateArray);
    sendBufferByte = 0;
    wantToSend = true;
  }
}
