//program writes byte defined as r_ID to position 0 in EEPROM, then transmits back written byte via serial

#define r_ID '!'      //robot's unique identifier byte
#include <EEPROM.h>  //include EEPROM library
void setup(){  
  Serial.begin(9600); //set up serial comm
  EEPROM.write(0, r_ID);  //write id to EERROM position 0
}
void loop(){
  //read position 0 and write to serial to confirm success of byte written
  Serial.write(EEPROM.read(0)); 
  delay(3000);
} 